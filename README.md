# Welcome to the XGUI public repository!
![enter image description here](http://630studios.com/wp-content/uploads/2018/02/forum_header.png)


This is the public repository for XGUI used for issue tracking and hosting the public demo of XGUI.

Forum Post: [Unity Forums](https://forum.unity.com/threads/xgui-ui-design-so-easy-anyone-can-do-it.486776/)

Twitter: [https://twitter.com/630Studios](https://twitter.com/630Studios)

Facebook: [630Studios](https://www.facebook.com/630Studios)

Website: [XGUI Website](http://630studios.com/products/xgui/)

# Requirements
XGUI requires Unity 2017.1 or newer

# Issues
Please report all issues with XGUI in the repos issue tracker. This is the best way for me to keep track of issues, asses what is most important to fix, and inform others when bugs have been fixed.